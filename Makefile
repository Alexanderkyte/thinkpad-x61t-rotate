prefix = /usr
CC := gcc 

OBJECTS := rotate normalize

install:
	install -m 0755 rotate $(prefix)/bin
	install -m 0755 normalize $(prefix)/bin


uninstall:
	rm $(prefix)/bin/rotate
	rm $(prefix)/bin/normalize

